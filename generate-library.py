import eyed3
import argparse
from shutil import copyfile
import os

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--source-file",
                    help="The source music file to copy", default="./in.mp3")
parser.add_argument("-c", "--count", type=int,
                    help="The elements that need to be generated", default=10000)
parser.add_argument("-a", "--albums", type=int,
                    help="Amount of albums to create", default=1000000)
parser.add_argument("-o", "--output", help="The output directory",
                    default="./output")
parser.add_argument("-v", "--verbose", action="store_true",
                    help="Enable verbose output")
args = parser.parse_args()

if args.verbose:
    print(27 * "-")
    print("Arguments")
    print(args)

if not os.path.exists(args.output):
    os.mkdir(args.output)

for i in range(args.count):
    output_file = os.path.join(args.output, "{}.mp3".format(i))
    copyfile(args.source_file, output_file)
    mp3 = eyed3.load(output_file)
    mp3.tag.artist = u"Artist {}".format(i)
    mp3.tag.album = u"Album {}".format(i % args.albums)
    mp3.tag.title = u"Title {}".format(i)
    mp3.tag.save()
    if args.verbose:
        print("Finished {}: {} - {} - {}".format(output_file,
                                                 mp3.tag.artist,
                                                 mp3.tag.album,
                                                 mp3.tag.title))

